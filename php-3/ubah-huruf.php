
<?php
function ubah_huruf($string){
    $output = "";
    for($i=0; $i<strlen($string); $i++){
        $position = strpos($string,$string[$i]);
        $output .= substr($string, $position + 1,1);
    }
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>