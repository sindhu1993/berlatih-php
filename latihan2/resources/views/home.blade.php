<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Latihan HTML</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <h1><b>SanberBook</b></h1>
    <h2><b>Social Media Developer Berkualitas</b></h2>
    <p>Belajar dan Berbagi agar hidup semakin santai berkualitas</p>
    <h3><b>Benefit join di SanberBook</b></h3>
    <ul>
        <li>Mendapatkan motivasi dar sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>
    <h3><b>Cara Bergabung ke SanberBook</b></h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Medaftar <a href="{{ route('/register') }}">Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
</body>
</html>
