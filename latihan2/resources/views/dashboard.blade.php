<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Latihan HTML</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <h1><b>SELAMAT DATANG {{ $first_name}} {{ $last_name }}</b></h1>
    <h2><b>Terima Kasih telah bergabung di Sanberbook. Social Media kita bersama!</b></h2>
</body>
</html>
