<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Latihan HTML</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <h1><b>Buat Account Baru</b></h1>
    <h2><b>Sign Up Form</b></h2>
    <form method="POST" action="{{ route('/welcome') }}">
        @csrf
    <p>First name :</p>
    <input type="text" name="first_name" autocomplete="off"
    required oninvalid="this.setCustomValidity('Enter Username')"
    oninput="this.setCustomValidity('')">
    <p>Last name :</p>
    <input type="text" name="last_name" autocomplete="off"
    required oninvalid="this.setCustomValidity('Enter Last name')"
    oninput="this.setCustomValidity('')">
    <p>Gender:</p>
    <input type="radio" id="male" name="gender" value="male"
    required oninvalid="this.setCustomValidity('Pick Gender')"
    oninput="this.setCustomValidity('')">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label>
    <p>Nationality</p>
    <select id="nationality" name="nationality"
    required oninvalid="this.setCustomValidity('Pick Nationality')"
    oninput="this.setCustomValidity('')">
        <option value="Indonesian">Indonesian</option>
        <option value="USA">USA</option>
        <option value="Spain">Spain</option>
        <option value="Japan">Japan</option>
    </select>
    <p>Language Spoken:</p>
    <input type="checkbox" id="language1" name="language1" value="Bahasa Indonesia">
    <label for="language1"> Bahasa Indonesian</label><br>
    <input type="checkbox" id="language2" name="language2" value="English">
    <label for="language2"> English</label><br>
    <input type="checkbox" id="language3" name="language3" value="Other">
    <label for="language3"> Other</label>
    <p>Bio :</p>
    <textarea name="bio" style="width:200px; height:100px;"
    required oninvalid="this.setCustomValidity('Enter Bio')"
    oninput="this.setCustomValidity('')"></textarea>
    <br>
    <button type="submit" value="submit" name="submit">Sign Up</button>
    </form>
</body>
</html>
