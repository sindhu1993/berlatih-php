@extends('adminlte.master')
@section('content')



<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Cast</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">List Cast</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


<section class="content">
    <div class="card card-dark">
        <div class="card-header">
          <h3 class="card-title">Form Add Data</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{ route('/cast') }}" method="POST">
            @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama</label>
              <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama" name="nama">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Umur</label>
              <input type="number" min="0" class="form-control" id="exampleInputPassword1" name="umur" placeholder="Umur">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Bio</label>
                <textarea class="form-control" name="bio"></textarea>
              </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
</section>





@endsection
