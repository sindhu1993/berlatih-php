@extends('adminlte.master')
@section('content')



<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>CRUD CAST</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">List Cast</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


<section class="content">
    <div class="row">
        <div class="col-md-4 mb-2">
            <a class="btn btn-primary" href="{{ route('/cast/create') }}">Add Data</a>
        </div>
    </div>
    <table class="table table-bordered">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($casts as $key => $x)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $x->nama }}</td>
                <td>{{ $x->umur }}</td>
                <td>{{ $x->bio }}</td>
                <td>
                    <a href="/cast/{{ $x->id }}/edit" class="btn btn-sm btn-warning">Edit</a>
                    {{-- <a href="/cast/{{ $x->id }}" class="btn btn-sm btn-danger">Delete</a> --}}
                    <form action="/cast/{{ $x->id }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-sm btn-danger">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</section>





  @endsection
