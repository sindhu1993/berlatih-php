<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index');
Route::get('/register', ['as'=>'/register','uses'=>'AuthController@register']);

Route::post('/welcome', ['as'=>'/welcome','uses'=>'AuthController@welcome']);

Route::get('/table', 'HomeController@table');
Route::get('/data-table', 'HomeController@datatable');


Route::get('/cast', ['as'=>'/cast','uses'=>'CastController@index']);
Route::get('/cast/create', ['as'=>'/cast/create','uses'=>'CastController@create']);
Route::post('/cast', ['as'=>'/cast','uses'=>'CastController@store']);
Route::get('/cast/{id}/edit','CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');

Route::delete('/cast/{cast_id}', 'CastController@destroy');
