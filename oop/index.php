<?php
// require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');
$sheep = new Animal("shaun");

echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "Legs : ".$sheep->legs."<br>"; // 4
echo "cold blooded : ".$sheep->cold_blooded."<br>"; // "no"

echo "<br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$frog = new Frog("buduk");
echo "Name : ".$frog->name."<br>";
echo "Legs : ".$frog->legs."<br>";
echo "cold blooded : ".$frog->cold_blooded."<br>";
echo $frog->jump()."<br>";

echo "<br>";

$ape = new Ape("kera sakti");
echo "Name : ".$ape->name."<br>";
echo "Legs : ".$ape->legs."<br>";
echo "cold blooded : ".$ape->cold_blooded."<br>";
echo $ape->yell()."<br>";

?>